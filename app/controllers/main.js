const renderDanhSachGiaoVienService = function () {
  giaoVienServ
    .layDanhSach()
    .then(function (res) {
      xuatDanhSachGiaoVien(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
};

renderDanhSachGiaoVienService();
const xoaGiaoVienServ = function (id) {
  axios({
    url: `${BASE_URL}/quanlynguoidung/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderDanhSachGiaoVienService();
    })
    .catch((err) => {
      console.log(err);
    });
};

const xuatDanhSachGiaoVien = function (dsgv) {
  var contentHTML = "";

  dsgv?.forEach(function (item) {
    var contentTr = `
        <tr>
        <td>${item.id}</td>
        <td>${item.taiKhoan}</td>
        <td>${item.matKhau}</td>
        <td>${item.hoTen}</td>
        <td>${item.email}</td>
        <td>${item.ngonNgu}</td>
        <td>${item.loaiND}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaGiaoVienServ(${item.id})">xoa</button>
        <button
         onclick="layThongTinGiaoVien(${item.id})"
        data-toggle="modal"
        data-target="#myModal"
         class="btn btn-success">sua</button>
        </td>
        </tr>
    `;

    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};

const layThongTinTuForm = function () {
  let taiKhoan = document.getElementById("TaiKhoan").value;
  let hoTen = document.getElementById("HoTen").value;
  let matKhau = document.getElementById("MatKhau").value;
  let email = document.getElementById("Email").value;
  let hinhAnh = document.getElementById("HinhAnh").value;
  let loaiND = document.getElementById("loaiNguoiDung").value;
  let ngonNgu = document.getElementById("loaiNgonNgu").value;
  let moTa = document.getElementById("MoTa").value;
  return new GiaoVien(
    taiKhoan,
    hoTen,
    matKhau,
    email,
    ngonNgu,
    moTa,
    hinhAnh,
    loaiND
  );
};

let validatorGv = new ValidatorGv();
const themMoiGV = function () {
  let newGv = layThongTinTuForm();
  let isValid = true;
  let isValidTaiKhoanGv =
    validatorGv.kiemTraRong("TaiKhoan", "tbTaiKhoan", "Tài khoản chưa nhập!") &&
    validatorGv.taiKhoanHopLe("TaiKhoan", "tbTaiKhoan");
  let isValidHoTenGv =
    validatorGv.kiemTraRong("HoTen", "tbHoTen", "Họ tên chưa nhập!") &&
    validatorGv.hoTenHopLe("HoTen", "tbHoTen");
  let isValidMatKhau =
    validatorGv.kiemTraRong("MatKhau", "tbMatKhau", "Mật khẩu chưa nhập!") &&
    validatorGv.matKhauHopLe("MatKhau", "tbMatKhau");
  let isValidEmail =
    validatorGv.kiemTraRong("Email", "tbEmail", "Email chưa nhập!") &&
    validatorGv.emailHopLe("Email", "tbEmail");
  let isValidHinhAnh =
    validatorGv.kiemTraRong("HinhAnh", "tbHinhAnh", "Hình ảnh chưa nhập!") &&
    validatorGv.hinhAnhHopLe("HinhAnh", "tbHinhAnh");
  let isValidNgonNgu = validatorGv.ngonNguHopLe("loaiNgonNgu", "tbNgonNgu");
  isValid =
    isValidTaiKhoanGv &&
    isValidHoTenGv &&
    isValidMatKhau &&
    isValidEmail &&
    isValidHinhAnh &&
    isValidNgonNgu;

  if (isValid == true) {
    axios({
      url: `${BASE_URL}/quanlynguoidung`,
      method: "POST",
      data: newGv,
    })
      .then((res) => {
        $("#myModal").modal("hide");
        renderDanhSachGiaoVienService();
        console.log("res", res);
      })
      .catch((err) => {
        console.log("err", err);
      });
  } else {
    console.log("sai roi");
  }
};

const renderThongTinLenForm = function (data) {
  document.getElementById("TaiKhoan").value = data.taiKhoan;
  document.getElementById("HoTen").value = data.hoTen;
  document.getElementById("MatKhau").value = data.matKhau;
  document.getElementById("Email").value = data.email;
  document.getElementById("HinhAnh").value = data.hinhAnh;
  document.getElementById("loaiNguoiDung").value = data.loaiND;
  document.getElementById("loaiNgonNgu").value = data.ngonNgu;
  document.getElementById("MoTa").value = data.moTa;
};

const layThongTinGiaoVien = function (id) {
  axios({
    url: `${BASE_URL}/quanlynguoidung/${id}`,
    method: "GET",
  })
    .then(function (res) {
      renderThongTinLenForm(res.data);
      document.querySelector("#id-gv span").innerHTML = res.data.id;
    })
    .catch(function (err) {
      console.log(err);
    });
};

const capNhatGv = function () {
  var updatedGv = layThongTinTuForm();
  let idGv = document.querySelector("#id-gv span").innerHTML * 1;
  axios({
    url: `${BASE_URL}/quanlynguoidung/${idGv}`,
    method: "PUT",
    data: updatedGv,
  })
    .then(function (res) {
      $("#myModal").modal("hide");
      renderDanhSachGiaoVienService();
    })
    .catch(function (err) {
      console.log(err);
    });
};
