const GiaoVien = function (
  _taiKhoan,
  _hoTen,
  _matKhau,
  _email,
  _ngonNgu,
  _moTa,
  _hinhAnh,
  _loaiND
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.matKhau = _matKhau;
  this.email = _email;
  this.ngonNgu = _ngonNgu;
  this.moTa = _moTa;
  this.hinhAnh = _hinhAnh;
  this.loaiND = _loaiND;
};
