function ValidatorGv() {
  // kiểm tra rỗng
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  // kiểm tra id không trùng
  //   this.kiemTraIdHopLe = function (newGv, danhSachGiaoVien) {
  //     let index = danhSachGiaoVien.findIndex((item) => {
  //       return item.taiKhoan == newGv.taiKhoan;
  //     });
  //     if (index == -1) {
  //       document.getElementById("tbTaiKhoan").innerText = "";
  //       return true;
  //     }
  //     document.getElementById("tbTaiKhoan").innerText =
  //       "Tài khoản giáo viên không được trùng";
  //     return false;
  //   };
  this.taiKhoanHopLe = function (idTarget, idError) {
    let parten = /^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Tên tài khoản chỉ chứa các ký tự chữ và số, gạch dưới và dấu chấm!";
  };
  this.hoTenHopLe = function (idTarget, idError) {
    let parten = /^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Tên nhân viên phải là chữ";
  };
  this.matKhauHopLe = function (idTarget, idError) {
    let parten =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Mật khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
  };
  this.emailHopLe = function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail.com$/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Email không hợp lệ";
  };
  this.hinhAnhHopLe = function (idTarget, idError) {
    let parten = /.(?:jpg|gif|png)$/;
    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Hình ảnh phải có định dạng jpg,png,gif";
  };
  this.ngonNguHopLe = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (
      valueInput == "ITALIAN" ||
      valueInput == "FRENCH" ||
      valueInput == "JAPANESE" ||
      valueInput == "CHINESE" ||
      valueInput == "RUSSIAN" ||
      valueInput == "SWEDEN" ||
      valueInput == "SPANISH"
    ) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Ngôn ngữ không hợp lệ!";
    return false;
  };
}
